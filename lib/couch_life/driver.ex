defmodule CouchLife.Driver do
  alias Couchdb.Connector
  alias Couchdb.Connector.Storage
  alias Couchdb.Connector.Writer
  alias Couchdb.Connector.Reader
  alias Couchdb.Connector.View

  def uuid() do
    UUID.uuid1()
  end

  def create_db(props) do
    Storage.storage_up(props)
  end

  def destroy_db(props) do
    Storage.storage_down(props)
  end

  def write(data, props) do
    Connector.create(props, data, uuid())
  end

  def read(id, props) do
    Connector.get(props, id)
  end

  def update(id, data, props) do
    Connector.update(props, data)
  end

  def delete(id, props) do
    Connector.destroy(props, id)
  end

  # ---------------------------------------------------------
  # json interface

  def write_json(json, props) do
    Writer.create(props, to_json(json), uuid())
  end

  def read_json(id, props) do
    Reader.get(props, id)
  end

  def update_json(id, json, props) do
    Writer.update(props, to_json(json), id)
  end

  def delete_json(id, props) do
    Couchdb.Connector.Writer.destroy(props, id)
  end

  def to_json(data) do
    case is_bitstring(data) do
      true  -> data
      false -> Poison.encode!(data)
    end
  end

  # ---------------------------------------------------------
  # # Example of getting a view to work
  # view = """
  # {
  #   "_id" : "_design/example",
  #   "views" : {
  #     "by_name" : {
  #       "map" : "function(doc){ emit(doc.id, doc) }"
  #     }
  #   }
  # }
  # """
  # Couchdb.Connector.View.create_view(db_props, "example", view)
  # Couchdb.Connector.View.fetch_all(db_props, "example", "by_name")


  def create_view(design_name, view_name, func_str, props) do
    json = view_json(design_name, view_name, func_str)
    View.create_view(props, design_name, json)
  end

  def view_json(design_name, view_name, func_str) do
    """
    {
      "_id" : "_design/#{design_name}",
      "views" : {
        "#{view_name}" : {
          "map" : "#{func_str}"
        }
      }
    }
    """
  end

  def fetch_all_by_key(design, view, key, props) do
    props
      |> Couchdb.Connector.UrlHelper.view_url(design, view)
      |> Couchdb.Connector.UrlHelper.query_path(key, :ok)
      |> HTTPoison.get!()
      |> Couchdb.Connector.ResponseHandler.handle_get()
  end

  def fetch_all(design_name, view_name, props) do
    # views should be created separately, but leave this here 
    # just as a reminder of the process
    # create_view(design_name, view_name, func_str, props)
    View.fetch_all(props, design_name, view_name)
  end

  def fetch_all(design_name, view_name, func_str, props) do
    create_view(design_name, view_name, func_str, props)
    fetch_all(design_name, view_name, props)
  end

end
