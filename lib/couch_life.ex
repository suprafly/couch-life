defmodule CouchLife do

  @response   :map
  @err_resp   {:error, :invalid_response}
  @env        Application.get_env(:couch_life, :couchdb_props)
  @protocol   Keyword.get(@env, :protocol)
  @db         Keyword.get(@env, :database )
  @host       Keyword.get(@env, :hostname)
  @port       Keyword.get(@env, :port)
  @user       Keyword.get(@env, :user, "admin")
  @password   Keyword.get(@env, :password, "")
  @props %{
    protocol: @protocol, 
    hostname: @host, 
    database: @db, 
    port:     @port,
    user:     @user,
    password: @password}

  alias CouchLife.Driver

  def props(opts \\ [])

  def props([]) do
    @props
  end

  def props(opts) do
    Enum.reduce(opts, @props, fn({k, v}, acc) -> Map.put(acc, k, v) end)
  end

  def create_db(props \\ @props) do
    Driver.create_db(props)
  end

  def destroy_db(props \\ @props) do
    Driver.destroy_db(props)
  end

  def fetch_all() do
    url = "#{@protocol}://#{@hostname}:#{@port}/#{@db}/_all_docs"
    nil
  end

  #--------------------------------------------------
  # We could do it like this...
  #--------------------------------------------------
  # def write(data, opts \\ []) do
  #   props = Keyword.get(opts, :props, @props)
  #   case Keyword.get(opts, :response, @response) do
  #     :map  -> Driver.write(data, props)
  #     :json -> Driver.write_json(data, props)
  #     _     -> @err_resp
  #   end
  # end
  #--------------------------------------------------

  def write(data, props \\ @props, opts \\ []) do
    case Keyword.get(opts, :response, @response) do
      :map  -> Driver.write(data, props)
      :json -> Driver.write_json(data, props)
      _     -> @err_resp
    end
  end

  def read(id, props \\ @props, opts \\ []) do
    case Keyword.get(opts, :response, @response) do
      :map  -> Driver.read(id, props)
      :json -> Driver.read_json(id, props)
      _     -> @err_resp
    end
  end

  def update(id, data, props \\ @props, opts \\ []) do
    case Keyword.get(opts, :response, @response) do
      :map  -> Driver.update(id, data, props)
      :json -> Driver.update_json(id, data, props)
      _     -> @err_resp
    end
  end

  def delete(id, props \\ @props, opts \\ []) do
    case Keyword.get(opts, :response, @response) do
      :map  -> Driver.delete(id, props)
      :json -> Driver.delete_json(id, props)
      _     -> @err_resp
    end
  end

  defp _try_response(map_func, json_func, props, opts) do
    case Keyword.get(opts, :response, @response) do
      :map  -> map_func.(data, props)
      :json -> json_func.(data, props)
      _     -> @err_resp
    end
  end

end
